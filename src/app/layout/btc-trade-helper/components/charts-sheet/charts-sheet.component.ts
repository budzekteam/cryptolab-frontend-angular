import {Component, OnInit} from '@angular/core';
import {BitbayService} from "../../../../shared/services/bitbay/bitbay.service";
import {StockService} from "../../../../shared/services/stock/stock.service";
import * as moment from 'moment';
import {Duration, Moment} from 'moment';

@Component({
    selector: 'app-charts-sheet',
    templateUrl: './charts-sheet.component.html',
    styleUrls: ['./charts-sheet.component.scss']
})
export class ChartsSheetComponent implements OnInit {

    constructor(private stockService: StockService, private bitbayService: BitbayService) {
    }

    /**
     * Rx question. How to have 2 observables and convert them to one, depending which one starts emitting first - take all elements from this observable
     */
    ngOnInit() {
        let c1: string = 'BTC', c2: string = 'NXT', c3: string = 'ETH',
            start: Moment = moment().subtract(7, 'days'),
            end: Moment = moment(),
            candleDuration: Duration = moment.duration(2, 'hours');

        this.stockService.historyWeightedAverage(c1, c3, start, end, candleDuration)//! poloniex; candlestick period in seconds; valid values are 300, 900, 1800, 7200, 14400, and 86400
            .subscribe(data => this.pushData({data: data, label: `${c1}-${c3}`, yAxisID: "y-axis-1",}));

        this.stockService.history(c1, c2, start, end, candleDuration)
            .subscribe(data => {
                let price = data.map(d => d.weightedAverage);
                let dateLabels = data.map(d => moment.unix(d.date).format("MM/DD/YYYY hh:mm:ss"));

                this.lineChartLabels = dateLabels;

                this.pushData({data: price, label: `${c1}-${c2}`, yAxisID: "y-axis-0",});

                console.log(this.lineChartLabels)
                console.log(this.lineChartData)
            });
    }

    private pushData(data: any) {
        if (!this.lineChartData)
            this.lineChartData = [];
        this.lineChartData.push(data);
    }

    public lineChartData: Array<any>;
    public lineChartLabels: Array<any>;
    public lineChartOptions: any = {
        responsive: true,
        scales: {
            yAxes: [{
                position: "left",
                "id": "y-axis-0"
            }, {
                position: "right",
                "id": "y-axis-1"
            }]
        }
    };

    public lineChartColors: Array<any> = [
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        },
        { // dark grey
            backgroundColor: 'rgba(77,83,96,0.2)',
            borderColor: 'rgba(77,83,96,1)',
            pointBackgroundColor: 'rgba(77,83,96,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(77,83,96,1)'
        },
        { // grey
            backgroundColor: 'rgba(148,159,177,0.2)',
            borderColor: 'rgba(148,159,177,1)',
            pointBackgroundColor: 'rgba(148,159,177,1)',
            pointBorderColor: '#fff',
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: 'rgba(148,159,177,0.8)'
        }
    ];
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';

    // events
    public chartClicked(e: any): void {
        // console.log(e);
    }

    public chartHovered(e: any): void {
        // console.log(e);
    }


}
