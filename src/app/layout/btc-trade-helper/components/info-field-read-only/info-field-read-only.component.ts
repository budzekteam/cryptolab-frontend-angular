import {Component, EventEmitter, forwardRef, Input, OnInit, Output} from '@angular/core';
import {Observable} from "rxjs/Rx";
import {InfoFieldComponent} from "../info-field/info-field.component";
import {NG_VALUE_ACCESSOR} from "@angular/forms";

const NUMBER_CONTROL_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => InfoFieldReadOnlyComponent),
    multi: true
};

@Component({
    selector: 'app-info-field-read-only',
    templateUrl: './info-field-read-only.component.html',
    styleUrls: ['./info-field-read-only.component.scss'],
    providers: [NUMBER_CONTROL_ACCESSOR]
})
export class InfoFieldReadOnlyComponent<T> extends InfoFieldComponent<T> implements OnInit {

    @Input() stream$: Observable<T>;
    @Input() beforeText: string;
    @Input() afterText: string;

    constructor() {
        super();
        this.locked = true;
    }

    ngOnInit() {
    }

}
