import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoFiledWithOptionalStreamComponent } from './info-filed-with-optional-stream.component';

describe('InfoFiledWithOptionalStreamComponent', () => {
  let component: InfoFiledWithOptionalStreamComponent;
  let fixture: ComponentFixture<InfoFiledWithOptionalStreamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoFiledWithOptionalStreamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoFiledWithOptionalStreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
