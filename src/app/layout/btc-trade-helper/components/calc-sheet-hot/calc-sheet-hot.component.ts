import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {getBasicData} from "./data";
import {CalcSheetRowModel} from "../calc-sheet/calc-sheet-row/calc-sheet-row.component";
import {HotTable} from "ng2-handsontable";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-calc-sheet-hot',
  templateUrl: './calc-sheet-hot.component.html',
  styleUrls: ['./calc-sheet-hot.component.scss']
})
export class CalcSheetHotComponent implements OnInit {

    @Input() positions: Array<CalcSheetRowModel>;
    @ViewChild('hot') hot: HotTable;

    constructor() { }

    ngOnInit() {
    }

    private data: any[] = this.positions;
    // private data: any[] = Observable.from(this.positions).map(p => [p.ilosc, p.bid, p.cenaBid, p.minAsk, p.ask, p.zysk]).toArray();
    private colHeaders: string[] = ["Ilość [BTC]","Kurs kupna [PLN]","Cena [PLN]","Min. kurs z zyskiem [PLN]","Kurs sprzedaży [PLN]","Zysk [PLN]"];
    private columns: any[] = [
        // {
        //     data: 'id'
        // },
        {
            data: 'ilosc'
        },
        {
            data: 'bid',
            // renderer: 'text',
            // readOnly: true
        },
        {
            data: 'cenaBid',
            // readOnly: true
        },
        {
            data: 'minAsk'
        },
        {
            data: 'ask',
            // source: 'product.options',
            // optionField: 'description',
            // type: 'autocomplete',
            // strict: false,
            // visibleRows: 4
        },
        {
            // data: 'zysk',
            type: 'numeric',
            formulas: true
            // format: '$ 0,0.00'
        }
        //,
        // {
        //     data: 'isActive',
        //     type: 'checkbox',
        //     checkedTemplate: 'Yes',
        //     uncheckedTemplate: 'No'
        // }
    ];
    private colWidths: number[] = [null, null, null, null, null, null, 30];
    private options: any = {
        // stretchH: 'all',
        columnSorting: true,
        contextMenu: [
            'row_above', 'row_below', 'remove_row'
        ],
        formulas: true
    };

    private afterChange(e: any) {
        console.log(e);
    }

    private afterOnCellMouseDown(e: any) {
        console.log(e);
    }



}
