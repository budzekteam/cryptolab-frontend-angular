import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcSheetHotComponent } from './calc-sheet-hot.component';

describe('CalcSheetHotComponent', () => {
  let component: CalcSheetHotComponent;
  let fixture: ComponentFixture<CalcSheetHotComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcSheetHotComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcSheetHotComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
