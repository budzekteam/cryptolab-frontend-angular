import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {Observable, Subject} from "rxjs/Rx";
import * as _ from "lodash";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import * as moment from 'moment';
import {ReplaySubject} from "rxjs/ReplaySubject";
// import _ from "lodash";

export interface Ticker {
    max: number;
    min: number;
    last: number;
    bid: number;
    ask: number;
    vwap: number;
    average: number;
    volume: number;
}

export interface Trade {
    date: number;
    price: number;
    amount: number;
    tid: string;
}

export var x;

@Injectable()
export class BitbayService {
    // TODO move all to config, which gets injected
    // private tickerUrl: string = "https://bitbay.net/API/Public/BTCPLN/ticker.json";
    private tickerUrl = (c1 = 'BTC', c2 = 'PLN') => `https://bitbay.net/API/Public/${c1 + c2}/ticker.json`;
    private tradesUrl = (c1 = 'BTC', c2 = 'PLN', tid: number, sort = 'desc') => `https://bitbay.net/API/Public/${c1 + c2}/trades.json?sort=${sort}${tid ? '&since=' + tid : ''}`;
    private lastTradeUrl = (c1 = 'BTC', c2 = 'PLN') => `https://bitbay.net/API/Public/${c1 + c2}/trades.json?sort=desc`;
    private requestInterval: number = 500;//500; //

    private currentLastMap: Map<string, Map<string, Observable<number>>> = new Map();

    private currentLastPlnBtc$: Subject<number>;
    private currentAskPlnBtc$: Subject<number>;
    private currentBidPlnBtc$: Subject<number>;


    constructor(private http: Http) {
        this.currentLastPlnBtc$ = new Subject();
        this.currentAskPlnBtc$ = new Subject();
        this.currentBidPlnBtc$ = new Subject();

        this.init();
    }

    private init() {
        Observable.interval(this.requestInterval).flatMap(() => this.http.get(this.tickerUrl('BTC', 'PLN'))).map((response) => <Ticker>response.json()).subscribe((t) => {
            this.currentLastPlnBtc$.next(t.last);
            this.currentAskPlnBtc$.next(t.ask);
            this.currentBidPlnBtc$.next(t.bid);
        });

        // this.currentLastPlnBtc$.subscribe(exc => console.log(exc));
    }

    public currentExchangePlnBtc(): Observable<number> {
        return this.currentLastPlnBtc$;
    }


    public addOrGetCurrentExchange(c1: string, c2: string) {
        let m1 = this.currentLastMap.get(c1);
        if (m1) {
            let m2 = m1.get(c2);
            if (!m2) {
                m2 = this.createCurrentExchange$(c1, c2);
                m1.set(c2, m2);
            }
            return m2;
        } else {
            this.currentLastMap.set(c1, new Map<string, Observable<number>>());
        }
    }

    createCurrentExchange$(c1: string, c2: string): Observable<number> {
        let source = new Subject();
        //TODO make it pausable
        Observable.interval(this.requestInterval).flatMap(() => this.http.get(this.tickerUrl(c1, c2))).map((response) => <Ticker>response.json()).subscribe((t) => {
            source.next(t.last);
        });
        return source;
    }


    public currentExchange(c1: string, c2: string): Observable<number> {
        //TODO this lodash way isn't working - returning weird object
        // let x = _(this.currentLastMap).map(m => m.get(c1)).map(m => m.get(c2));
        // console.log(x.value());

        return this.addOrGetCurrentExchange(c1, c2);
    }


    private historyRequestRange = 50;

    public history(c1: string, c2: string) {

        let since = moment().subtract(7, "days").unix();//.valueOf();

        let trades$ = new ReplaySubject<Trade>()



        //
        let source = Observable
            .of(this.lastTradeUrl(c1, c2))
            .do(url => console.log(`Obtaining last tid from ${url}`))
            .flatMap(url => this.http.get(url))
            .flatMap((response) => <Array<Trade>>response.json())
            .do((t) => {
                console.log(t);
            })
            .take(1)
            .map(trade => Number(trade.tid))
            .do(lastTid => console.log(`Last tid: ${lastTid}`))
            .subscribe(tid => this.getHistorySince(c1, c2, tid, since, trades$),
                err => console.log(err),
                ()=>{});

        return trades$;
    }

    getTrades$(c1: string, c2: string, tid: number){
        return Observable
            .of(this.tradesUrl(c1, c2, tid))
            .do(url => console.log(`Obtaining last tid from ${url}`))
            .flatMap(url => this.http.get(url))
            .flatMap((response) => <Array<Trade>>response.json());
    }

    getHistorySince(c1: string, c2: string, tid: number, since: number, trades$: Subject<Trade>){
        // Observable.range(, 1).flatMap(getTrades$())


        // let lastTid$ = new BehaviorSubject<number>(tid);
        // let stop = new Subject<boolean>();
        //
        // let test$ = new Subject<number>();
        // let lastT = null;
        //     test$.subscribe(t => {
        //             console.log("test$ - next");
        //             lastT = t;
        //             test$.next(lastT+1);
        //         },
        //     err => console.log(err),
        //     () => {
        //         console.log("test$ - complete - start");
        //         test$.next(lastT+1);
        //         console.log("test$ - complete - end");
        //     });
        //
        // test$.next(1);

        //
        // let src = Observable.range(1, tid).takeUntil(stop).do(x => {
        //     if(x > 10)
        //         stop.next(true);
        // }).flatMap(_ => this.getTrades$(c1, c2, lastTid$.getValue())).
        // subscribe(t => console.log(t));





//
//
//
//         console.log("Main subscryption");
//
//         tid = tid - this.historyRequestRange;
// x
//         // Rx.Observable.repeat().
//
//             this.getTrades$(c1, c2, tid).subscribe(
//                 (t) => {
//                     // if (t.date >= since)
//                     //     nextTradeTid$.complete();
//                     lastTid$.next(Number(t.tid));
//                     trades$.next(t);
//                 },
//                 (err) => {
//                     console.log(err);
//                 },
//                 () => {
//                     this.getHistorySince(c1, c2, lastTid$.getValue(), since, trades$);
//                 });

    }
}
